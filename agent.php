<?php

use Commando\Command;

chdir(__DIR__);

require 'vendor/autoload.php';

$config = include 'config/config.default.php';

$env = getenv('ENVIRONMENT') ?: 'local';
$envConfigFileName = 'config/config.' . $env . '.php';

if (file_exists($envConfigFileName)) {
    $envConfig = include $envConfigFileName;

    $config = array_replace_recursive($config, $envConfig);
}

$agentCmd = new Command();

$agentCmd
    ->option('a')
    ->aka('all')
    ->describe('When given, this will load all data from the sources, rather than just incremental.')
    ->boolean();

$agentCmd
    ->option('i')
    ->aka('inc')
    ->aka('incremental')
    ->describe('Do incremental updates. This is the default option.')
    ->boolean();

$agent = new \Updashd\Agent($config);

if ($agentCmd['all']) {
    $agent->setIncludeAll();
    $agent->runOnce();
}

if ($agentCmd['inc']) {
    $agent->setIncludeAll(false);
    $agent->run();
}
