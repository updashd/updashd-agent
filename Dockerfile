FROM updashd/php:7.2.13-cli-stretch

RUN mkdir -p /opt/updashd-agent

WORKDIR /opt/updashd-agent

COPY config ./config
COPY src ./src
COPY data ./data
COPY *.php ./
COPY composer.* ./

RUN curl https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer -q | php -- --quiet --install-dir="/usr/local/bin" --filename="composer" \
    && composer install

CMD ["php", "agent.php"]
