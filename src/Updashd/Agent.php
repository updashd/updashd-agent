<?php
namespace Updashd;

use Predis\Client;
use Updashd\DbEvent\PopulateStatement;
use Updashd\DbEvent\PopulateStatements;
use Updashd\DbEvent\Processor;
use Updashd\DbEvent\Subscriptions;
use Updashd\Scheduler\Model\Task;
use Updashd\Scheduler\Scheduler;
use Updashd\Scheduler\ScheduleType;
use Updashd\Scheduler\State;
use Updashd\Configlib\Config;

class Agent {
    const CLIENT_ID = 'SCH';
    const TMP_TABLE_NAME = 'tmp_sch';

    private $config;
    private $doctrineManager;
    private $schedulers = array();

    private $processor;

    private $includeAll = false;

    public function __construct ($config) {
        // Store the configuration
        $this->setConfig($config);

        $doctrineManager = new Doctrine\Manager($config['doctrine']);
        $this->setDoctrineManager($doctrineManager);
    }

    /**
     * @return int count of how many records were updated
     */
    private function process () {
        // Query the table for updates since the last check
        $changes = $this->getChanges();

        $count = 0;
        // For each changed row (one row for each node service zone record)
        foreach ($changes as $row) {
            $zoneId = $row['zone_id'];
            $nodeServiceZoneId = $row['node_service_zone_id'];
            $isEnabled = $row['is_enabled'];
            $scheduleType = $row['schedule_type'];
            $scheduleValue = $row['schedule_value'];
            $hostname = $row['hostname'];
            $ip = $row['ip'];
            $serviceName = $row['module_name'];
            $accountId = $row['account_id'];
            $config = $row['config'];

            $scheduler = $this->getScheduler($zoneId);

            // Get the task entity (this also creates one if it does not exist)
            $task = $scheduler->getTaskEntity($nodeServiceZoneId, true);

            $taskInactive = $this->isTaskInactive($task);

            // If it is disabled, just try to remove it
            if (! $isEnabled && ! $taskInactive) {
                // Remove the task
                $scheduler->removeTask($nodeServiceZoneId);
            }
            // Otherwise populate the fields
            else {
                $referenceFields = [
                    'hostname' => $hostname,
                    'ip' => $ip
                ];

                $config = Config::fillReferenceFields($config, $referenceFields);

                $task->setConfig($config);

                $task->setZone($zoneId);
                $task->setScheduleType($scheduleType);

                if ($scheduleType == ScheduleType::TYPE_CRON) {
                    $task->setCron($scheduleValue);
                }
                else if ($scheduleType == ScheduleType::TYPE_SECOND_INTERVAL) {
                    $task->setInterval($scheduleValue);
                }

                $task->setService($serviceName);
                $task->setEnabled($isEnabled);
                $task->setAccountId($accountId);

                // Save any changes
                $task->save();
            }

            // If the task is running and is still enabled still
            if ($isEnabled && $taskInactive) {
                // Add the task
                $scheduler->addTask($task);
            }

            $count++;
        }

        return $count;
    }

    /**
     * Run the worker
     * @param $running
     */
    public function run (&$running = true) {
        while ($running) {
            $this->runOnce();

            $sleepTime = (int) $this->getConfig('frequency');

            sleep($sleepTime);

            // Attempting to resolve memory leaks
            $this->getDoctrineManager()->getEntityManager()->clear();
        }
    }

    public function runOnce () {
        $count = $this->process();

        if ($count > 0) {
            printf("time: %s, count: %d\n", date('Y-m-d H:i:s'), $count);
        }
    }

    protected function getSubscriptions () {
        $subscriptions = new Subscriptions();

        $subscriptions->addSubscriptionAuto('node', ['I', 'U']);
        $subscriptions->addSubscriptionAuto('service', ['I', 'U']);
        $subscriptions->addSubscriptionAuto('zone', ['I', 'U']);
        $subscriptions->addSubscriptionAuto('node_service', ['I', 'U']);
        $subscriptions->addSubscriptionAuto('node_service_zone', ['I', 'U', 'D']);

        return $subscriptions;
    }

    protected function getProcessor () {
        if (! $this->processor) {
            $processor = new Processor($this->getPdo(), self::CLIENT_ID, self::TMP_TABLE_NAME, $this->getSubscriptions());
            $processor->setPopulateStatements($this->getPopulateStatements());
            $this->processor = $processor;
        }

        return $this->processor;
    }

    protected function getPopulateStatements () {
        $populateStatements = new PopulateStatements();
        $populateStatements
            ->addStatement(new PopulateStatement(
                'nsz.node_service_zone_id',
                'node',
                null,
                [
                    'INNER JOIN node n ON a.new_id = n.node_id',
                    'INNER JOIN node_service ns ON ns.node_id = n.node_id',
                    'INNER JOIN node_service_zone nsz ON nsz.node_service_id = ns.node_service_id',
                ],
                ['I', 'U']
            ))
            ->addStatement(new PopulateStatement(
                'nsz.node_service_zone_id',
                'service',
                null,
                [
                    'INNER JOIN service s ON a.new_id = s.service_id',
                    'INNER JOIN node_service ns ON ns.service_id = s.service_id',
                    'INNER JOIN node_service_zone nsz ON nsz.node_service_id = ns.node_service_id',
                ],
                ['I', 'U']
            ))
            ->addStatement(new PopulateStatement(
                'nsz.node_service_zone_id',
                'node_service',
                null,
                [
                    'INNER JOIN node_service ns ON a.new_id = ns.node_service_id',
                    'INNER JOIN node_service_zone nsz ON nsz.node_service_id = ns.node_service_id',
                ],
                ['I', 'U']
            ))
            ->addStatement(new PopulateStatement(
                'nsz.node_service_zone_id',
                'zone',
                null,
                [
                    'INNER JOIN node_service_zone nsz ON a.new_id = nsz.zone_id',
                ],
                ['I', 'U']
            ))
            ->addStatement(new PopulateStatement(
                'a.new_id',
                'node_service_zone',
                null,
                [],
                ['I', 'U']
            ))
            ->addStatement(new PopulateStatement(
                'a.old_id',
                'node_service_zone',
                null,
                [],
                ['D']
            ));

        return $populateStatements;
    }

    /**
     * Determine of the given task is active
     * @param Task $task
     * @return bool
     */
    public function isTaskInactive (Task $task) {
        $inactiveStates = [
            State::STATE_DISABLED,
            State::STATE_ERROR,
            State::STATE_UNDEFINED
        ];

        return in_array($task->getState(), $inactiveStates);
    }

    protected function getPdo () {
        $em = $this->getDoctrineManager()->getEntityManager();
        return $em->getConnection();
    }

    /**
     * This retrieves all records that have been updated since a certain date and time.
     * @return array
     */
    public function getChanges () {
        $processor = $this->getProcessor();

        // Populate the tmp table with the stuff we need to process
        $processor->populate();

        $changes = $processor->retrieve(
            [
                'ns.node_service_id',
                'z.zone_id',
                '(' .
                '  IFNULL(n.is_enabled, 0) &' .
                '  IFNULL(ns.is_enabled, 0) &' .
                '  IF(ns.node_service_id IS NOT NULL, 1, 0)' .
                ') AS is_enabled',
                's.module_name',
                'ns.schedule_type',
                'ns.schedule_value',
                'n.hostname',
                'n.ip',
                'ns.config',
                'n.node_id',
                'a.account_id',
            ],
            [
                'LEFT JOIN node_service_zone nsz ON r.root_id = nsz.node_service_zone_id',
                'LEFT JOIN node_service ns ON nsz.node_service_id = ns.node_service_id',
                'LEFT JOIN service s ON ns.service_id = s.service_id',
                'LEFT JOIN node n ON ns.node_id = n.node_id',
                'LEFT JOIN zone z ON nsz.zone_id = z.zone_id',
                'LEFT JOIN account a ON n.account_id = a.account_id',
            ],
            'node_service_zone_id',
            ($this->getIncludeAll() ? 'node_service_zone' : null)
        );

        $processor->finalize();

        return $changes;
    }

    /**
     * @param string $zoneId
     * @param Scheduler $scheduler
     */
    public function setScheduler ($zoneId, $scheduler) {
        $this->schedulers[$zoneId] = $scheduler;
    }

    /**
     * @param $zoneId
     * @return Scheduler|null
     */
    public function getScheduler ($zoneId) {
        // Create the scheduler if it does not already exist
        if (! array_key_exists($zoneId, $this->schedulers)) {
            $this->setScheduler($zoneId, $this->initializeScheduler($zoneId));
        }

        // Return the item
        return $this->schedulers[$zoneId];
    }

    /**
     * @param $zoneId
     * @return Scheduler
     * @throws \Exception
     */
    public function initializeScheduler ($zoneId) {
        $zonesConfig = $this->getConfig('zones');

        $zoneConfig = null;

        if (array_key_exists($zoneId, $zonesConfig)) {
            $zoneConfig = $zonesConfig[$zoneId];
        }
        else if (array_key_exists('default', $zonesConfig)) {
            $zoneConfig = $zonesConfig['default'];
        }
        else {
            throw new \Exception('Could not create scheduler for non-configured zone "' . $zoneId . '". Either the config must exist or there must be a "default" zone.');
        }

        if (! array_key_exists('predis', $zoneConfig)) {
            throw new \Exception('Predis key must exist in zone configuration. Please check your updashd-agent config syntax under zone: ' . $zoneId);
        }

        $client = new Client($zoneConfig['predis']);

        $scheduler = new Scheduler($client, $zoneId);

        return $scheduler;
    }

    /**
     * Retrieve the config, or a key from it.
     * @param string $key
     * @return array
     */
    public function getConfig ($key = null) {
        if ($key && array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }

        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getSchedulers () {
        return $this->schedulers;
    }

    /**
     * @param array $schedulers
     */
    public function setSchedulers ($schedulers) {
        $this->schedulers = $schedulers;
    }

    /**
     * @return Doctrine\Manager
     */
    public function getDoctrineManager () {
        return $this->doctrineManager;
    }

    /**
     * @param Doctrine\Manager $doctrineManager
     */
    public function setDoctrineManager ($doctrineManager) {
        $this->doctrineManager = $doctrineManager;
    }

    /**
     * @return bool
     */
    public function getIncludeAll () {
        return $this->includeAll;
    }

    /**
     * @param bool $includeAll
     */
    public function setIncludeAll ($includeAll = true) {
        $this->includeAll = $includeAll;
    }
}